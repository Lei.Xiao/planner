## Weekly Planner: March 16 - 20, 2020

- Work on the EPAM Project and fully understand the CICD process and modeling [GitLab Pages](https://gitlab.huskyenergy.com/husky-data-science/projects/epam_pcp_project_v2)
- Prepare presentation slides on the EPAM project
- Revisit the Atlantic Gas Allocation project [GitLab Pages](https://gitlab.huskyenergy.com/husky-data-science/projects/atlantic-gas-lift-analysis)
